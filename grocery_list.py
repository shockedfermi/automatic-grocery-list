#!/usr/bin/python3

"""
groceries

Usage:
./grocery_list [shop]

"""

import docopt
import string

LANGUAGE = ""
try:
    flan = open( "LANGUAGE_PREF.txt", "r" )
    LANGUAGE = flan.read().split()[0]
except FileNotFoundError:
    LANGUAGE = {"1": "FR", "2": "EN"}[ input( "Pick a language [1/2]:\n1: FR\n2: EN\n" ) ]
    flan = open( "LANGUAGE_PREF.txt", "w" )
    flan.write( LANGUAGE )

try:
    fstock = open( "STOCK_PREF.txt", "r" )
    stock = open( fstock.read().split()[0], "r" )
except FileNotFoundError:
    stockfile = input( "Enter stock file (press enter to use example stock file):\n" )
    if not stockfile:
        stockfile = "example_stock.txt"
    try:
        stock = open( stockfile, "r" )
    except FileNotFoundError:
        print("Error. Stock file not found. Please try again.")
        raise FileNotFoundError
    fstock = open( "STOCK_PREF.txt", "w" )
    fstock.write( stockfile )

location_dic = {}
grocery_list = []

for line in stock.readlines():

    if line[0] == "#":
        continue
    if line[0] == "-":
        category = line[1:-1]
        continue
    if not line.strip():
        continue

    if category == { "FR": 'ordre:', "EN": "order:" }[ LANGUAGE ]:
        for iloc, loc in enumerate( line.split(',') ):
            location_dic[loc.strip()] = iloc
        continue
    
    item, location = line.split(":")[0], line.split(":")[1]

    if input(category + { "FR": ": on a besoin de ", "EN": ": do we need " }[ LANGUAGE ] + item + "? [y/]") in set('yesoui'):
        grocery_list.append( ( item.strip(' \n'), location.strip(' \n') ) )
        
print( { "FR": "Voici ta liste:", "EN": "Here's your list:" }[ LANGUAGE ])
print( "\nITEM - LOCATION\n" )
for item in sorted( grocery_list, key=lambda food: location_dic[food[1]] ) :
    print( "{} - {}".format( item[0], item[1] ) )
